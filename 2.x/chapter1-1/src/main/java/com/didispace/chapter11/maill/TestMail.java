package com.didispace.chapter11.maill;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.mail.Mail;
import cn.hutool.extra.mail.MailAccount;

import javax.activation.DataSource;
import javax.activation.URLDataSource;
import java.util.ArrayList;
import java.util.List;

public class TestMail {
    private void sendEmail() {
        MailAccount mailAccount = new MailAccount();
        mailAccount.setAuth(true);
        mailAccount.setHost("smtp.163.com");
        mailAccount.setUser("发件邮箱号");
        mailAccount.setPass("授权码");
        mailAccount.setFrom("发件邮箱号");


        Mail mail = Mail.create(mailAccount).setUseGlobalSession(true);
        mail.setTos("接收邮箱号");
        mail.setTitle("标题");
        //content 内容
        //参数true 是否支持HTML，不支持false
        mail.setContent("内容", true);
        /**
         *添加附件
         *我这的url是阿里云oss服务的
         *buckentName ,endPoint 看公司oss的参数配置
         *key1,key2是   文件夹/文件名   例如：a.jpg在oss服务器名字为Java的bucket下面的picture
         *文件夹下，那么URL就是 https://Java.endPoint/picture/a.jpg
         */
        String[] urls = {
                "https://bucketName.endPoint/key1",
                "https://bucketName.endPoint/key2"};
        //构建文件数组
        List<DataSource> dataSources = new ArrayList<>();
        for (String url : urls) {
            DataSource dataSource = new URLDataSource(URLUtil.url(url));
            dataSources.add(dataSource);
        }
        //设置邮件附件
        mail.setAttachments(dataSources.toArray(new DataSource[]{}));
        String send = mail.send();
    }

}
