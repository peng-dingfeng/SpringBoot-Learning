package com.didispace.chapter11.maill;

import cn.hutool.extra.mail.MailUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;

public class SendMail {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${zhonghang.exportPath:D:\\test}")
    private String EXPORT_PATH;

    @Value("${zhonghang.importPath:D:\\transfer\\zip}")
    private String IMPORT_PATH;

    private final String ONE_WAY_EXPORT = "one_way_export";

    private final String B01_TEMP_PATH = File.separator + "testmail" + File.separator;

    private final String SUFFIX = ".json";

    public void sendMail (){
        //1、封账发送邮件文件
        long currentTime = System.currentTimeMillis();
        String tempFolder = EXPORT_PATH + B01_TEMP_PATH + currentTime + ONE_WAY_EXPORT + SUFFIX;
        File file = new File(tempFolder);
        String mailReturn = MailUtil.send("", "B01组织机构对比", "查询NW中b01和b01_temporary中单位不一致的情况",
                false, file);
        logger.info("调用邮件返回信息",mailReturn);
        return;
    }

}
